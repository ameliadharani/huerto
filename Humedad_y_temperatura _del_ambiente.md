#SENSOR DHT11

* El módulo DHT11 es un sensor digital de Temperatura y Humedad. 
* Utiliza un sensor capacitivo de humedad y un termistor para medir el aire circundante y solo un pin para la lectura de los datos. 
* Realiza una nueva lectura después de 2 segundos.
* Trabaja con un rango de medición de temperatura de 0 a 50 °C con precisión de ±2.0 °C y un rango de humedad de 20% a 90% RH con precisión de 4% RH. 
* Para utilizar el sensor es necesario descargar la librería DHT.
* Se puede descargar del siguiente enlace:
* https://github.com/adafruit/DHT-sensor-library

```
#include <DHT.h>

#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);
#include <Wire.h>
#include <Servo.h>
Servo myservo1;
Servo myservo2;
Servo myservo3;
int pos;

#define DHTPIN 2
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);
float h;
float t;
float f;

const int sensor_h1 = A0; // Pin para la lectura analógica
const int sensor_h2 = A1; // Pin para la lectura analógica
const int sensor_h3 = A2; // Pin para la lectura analógica
const int senal_bombeo = 9;  // Pin para la señal de bombeo
const int ledpin = 4;
int H1 = 0;           // Variables para almacenar la lectura de la humedad
int H2 = 0;
int H3 = 0;
int HUMBRAL = 300;    // humbral para la activar riego
int HISTERESIS = 20;

void setup() {
  Serial.begin(19200);
  Wire.begin();
  lcd.begin(16, 2);
  lcd.clear();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Hola Huerto!");
  lcd.setCursor(0, 1);
  lcd.print("Buenos Dias");
  // pinMode(ledPin, OUTPUT);
  pinMode(sensor_h1, INPUT);
  pinMode(sensor_h2, INPUT);
  pinMode(sensor_h3, INPUT);
  myservo1.attach(3);
  myservo2.attach(5);
  myservo3.attach(6);
  myservo1.write(130);
  myservo2.write(90);
  myservo3.write(90);
  pinMode(senal_bombeo, OUTPUT);
  dht.begin();
}

void Nivel1() {
  myservo2.write(110);
  delay(500);
  myservo3.write(50);
  delay(500);
}
void Nivel2() {
  myservo2.write(110);
  delay(500);
  myservo3.write(120);
  delay(500);
}
void Nivel3() {
  myservo2.write(140);
  delay(500);
  myservo3.write(115);
  delay(500);
}
void Movimiento() {
  int j = 0;
  int conteo = 4;
  while (j++ <= conteo) {
    for (pos = 49; pos <= 129; pos += 1) {
      myservo1.write(pos);
      delay(40);
    }
    for (pos = 130; pos >= 50; pos -= 1) {
      myservo1.write(pos);
      delay(40);
    }
  }
}
void Bombeo() {
  digitalWrite(senal_bombeo, HIGH);

}
void stop() {

  digitalWrite(senal_bombeo, LOW);
  myservo2.write(110);
  delay(500);
  myservo3.write(100);
  delay(500);
  myservo1.write(130);
  delay(500);
}
void loop() {
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  float f = dht.readTemperature(true);
  if (isnan(h) || isnan(t) || isnan(f)) {
    lcd.print("Error obteniendo los datos");
    delay(3000);
    lcd.clear();
    return;
  }
  float hif = dht.computeHeatIndex(f, h);
  float hic = dht.computeHeatIndex(t, h, false);

  H1 = analogRead(sensor_h1); //Lectura analógica Sensor 1
  H2 = analogRead(sensor_h2); //Lectura analógica Sensor 2
  H3 = analogRead(sensor_h3); //Lectura analógica Sensor 3

  if (H1 <= 300)
    Nivel1();
  Bombeo();
  Movimiento();
  if (H1 >= 700)
    stop();

  if (H2 <= 300)
    Nivel2();
  Bombeo();
  Movimiento();
  if (H2 >= 700)
    stop();

  if (H3 <= 300)
    Nivel3();
  Bombeo();
  Movimiento();
  if (H3 >= 700)
    stop();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Humedad");
  lcd.print(h);
  lcd.print(" %\t ");
  lcd.setCursor(0, 1);
  lcd.print("Temperatura");
  lcd.print(t);
  lcd.print(" °C ");
  delay(4000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Indice de calor");
  lcd.setCursor(0, 1);
  lcd.print(hic);
  lcd.print(" °C ");
  lcd.print(hif);
  lcd.print(" °F ");
  delay(4000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Humedad Nivel 1");
  lcd.setCursor(0, 1);
  lcd.print(H1);
  delay(4000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Humedad Nivel 2");
  lcd.setCursor(0, 1);
  lcd.print(H2);
  delay(4000);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Humedad Nivel 3");
  lcd.setCursor(0, 1);
  lcd.print(H3);
  delay(4000);
  lcd.clear();
```
 
  Serial.print("humedad Nivel 1 ");
  Serial.print(H1);
  delay(2500);
  Serial.print("Humedad Nivel 2");
  Serial.print(H2);
  delay(2500);
  Serial.print("Humedad Nivel 3");
  Serial.println(H3);
  delay(2500);
}

