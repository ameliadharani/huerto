## FC-28  Higrómetro

* El modulo FC-28 es un sensor de humedad del suelo o higrómetro. 
* Su funcionamiento permite medir la conductividad del suelo, es decir, si el suelo se encuentra muy húmedo será mayor su conductividad y si el suelo es muy seco la conductividad bajará. 
* El módulo FC-28 consta de una sonda que es la que se utiliza para medir la humedad y por una tarjeta que es  la encargada de dar la medición. 
* La tarjeta cuenta con un LM393 (comparador) y un potenciómetro.
* Cabe mencionar que la medición se puede entregar de manera análoga o digital.
```
/***
 * Conexiones
 * 
 * Arduino Nano
 * 
 * Sensor de humedad en tierra con salida analógica y digital
 * 
 * Sensor_1 | Arduino Nano
 * -------------------------
 * GND      | GND   
 * Vcc      | 5V
 * A0       | A0
 * 
 * Sensor_2 | Arduino Nano
 * ------------------------
 * GND      | GND   
 * Vcc      | 5V
 * A0       | A1
 * 
 * Sensor_3 | Arduino Nano
 * -------------------------
 * GND      | GND   
 * Vcc      | 5V
 * A0       | A2
 * 
 * 
 
/***
 * Descripción
 * 
 * Cuando la humedad es baja la lectura del sensor es alta (1024)
 * Al incrementarse la humedad la lectura de A0 disminuye.
 * 
 * 
 * 
 ***/

#include<Servo.h>
#include <ThresholdLib.h>

// Pines analógicos para sensores de humedad
const int sensor_h1 = A0; // Pin para la lectura analógica 
const int sensor_h2 = A1; // Pin para la lectura analógica 
const int sensor_h3 = A2; // Pin para la lectura analógica

// Pines digitales para Servos
const int servo1 = 3;
const int servo2 = 5;
const int servo3 = 6;

// Pin digital para la señal de bombeo
const int senal_bombeo = 9;

//Declaracion de variables
int H1 = 0;           // Variables para almacenar la lectura de la humedad
int H2 = 0;
int H3 = 0;
const int HUMBRAL = 512;    // umbral para la activar riego
const int HISTERESIS = 100;


Threshold<int> threshold1(HUMBRAL - HISTERESIS, HUMBRAL + HISTERESIS);
Threshold<int> threshold2(HUMBRAL - HISTERESIS, HUMBRAL + HISTERESIS);
Threshold<int> threshold3(HUMBRAL - HISTERESIS, HUMBRAL + HISTERESIS);

Servo myservo1;
Servo myservo2;
Servo myservo3;

int pos;

void setup() {
  Serial.begin(19200); // comunicación serie
  pinMode(senal_bombeo, OUTPUT);

  myservo1.attach(servo1);
  myservo2.attach(servo2);
  myservo3.attach(servo3);

  //Posicion inicial de los servos
  myservo1.write(130);
  myservo2.write(90);
  myservo3.write(90);
}
 
void loop() {
  
  H1 = analogRead(sensor_h1); //Lectura analógica Sensor 1
  H2 = analogRead(sensor_h2); //Lectura analógica Sensor 2
  H3 = analogRead(sensor_h3); //Lectura analógica Sensor 3

  bool FLAG_LEVEL1 = threshold1.AddValue(H1);
  bool FLAG_LEVEL2 = threshold2.AddValue(H2);
  bool FLAG_LEVEL3 = threshold3.AddValue(H3);    

  if ( FLAG_LEVEL1 ){
    Nivel1();
    Bombeo();
    Movimiento();
  }
  else
    stop();
    
  if ( FLAG_LEVEL2 ){
    Nivel2();
    Bombeo();
    Movimiento();    
  }
 else
    stop();
  
  if ( FLAG_LEVEL3 ){
    Nivel3();
    Bombeo();
    Movimiento();        
  }
  else
    stop();

// Para verificar los niveles de histéresis
//  Serial.print(H1);


//  Serial.print(",\t");
//  Serial.print(H2);
//  Serial.print(",\t");
//  Serial.print(H3);
//  Serial.print(",\t");
//  Serial.print(HUMBRAL-HISTERESIS);
//  Serial.print(",\t");
//  Serial.print(HUMBRAL+HISTERESIS);
//  Serial.print(",\t");
//  Serial.print(FLAG_LEVEL1*1024);
//  Serial.print(",\t");
//  Serial.print(FLAG_LEVEL2*1024);
//  Serial.print(",\t");
//  Serial.println(FLAG_LEVEL3*1024);
  
  delay(8000);
}

// Declaracion de funciones

void Nivel1() {
  myservo2.write(110);
  delay(500);
  myservo3.write(50);
  delay(500);
}
void Nivel2() {
  myservo2.write(110);
  delay(500);
  myservo3.write(120);
  delay(500);
}
void Nivel3() {
  myservo2.write(140);
  delay(500);
  myservo3.write(115);
  delay(500);
}
void Movimiento() {
  int count = 3;
  int i = 0;
  while(i++ <= count){
    for (pos = 49; pos <= 129; pos += 1) {
      myservo1.write(pos);
      delay(40);
    }
    for (pos = 130; pos >= 50; pos -= 1) {
      myservo1.write(pos);
      delay(40);
    }
  }
}
void Bombeo() {
  digitalWrite(senal_bombeo, HIGH);

}
void stop() {
  digitalWrite(senal_bombeo, LOW);   
  myservo2.write(110);
  delay(500);
  myservo3.write(100);
  delay(500);
  myservo1.write(130);
  delay(500);
}

```