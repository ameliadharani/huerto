# Huerto 

# Notas sobre el proyecto "Monitoreo de variables de Huerto"

# Objetivo

   Generar un modelo de huerto vertical para monitorear sus
   variables de humedad, luz y temperatura, via web.
   
   opcional: Enviar alarma a una aplicación.
   

# Material y Equipo

* *esp32*: Modulo WiFi y Bluetooth
* *Tiraled 12 V*
* *Sensor de humedad relativa HT11*
* *Sensor de humedad en tierra*
* *Modulo fotresistencia*
* *Bomba de agua sumergible*
* *Batería recargable de 12V*
* *RTC*: Reloj de tiempo *Nota:* Verificar la posibilidad de obtener la hora via red.

# Metodología

## Estructura

* Marco de herrería de 50 x 80 cm.
* Se agregaron tres pares de mensulas para contar con 3 niveles. 
* Cada nivel es a su vez un marco en el que se colocó una bolsa colgante que
contiene la tierra y las plantas.

## Sistema de riego 

Via bomba sumergible, se alimentará cada nivel cuando sea necesario.


## Sistema de Iluminación

El sistema de iluminacion se  realizará via tiras led y encenderá 
durante 12 horas desde las 7 hasta las 19 horas.

## Sistema de control (Monitoreo)

*Descripción:*

* Se activará una alarma cuando la humedad de la tierra sea baja.
* Se encenderá la iluminación a las 7 horas y se apagará a las 19 horas.
* Se mostrará la temperatura del sistema.




