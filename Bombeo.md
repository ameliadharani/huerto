# BOMBA DE AGUA SUMERGIBLE 


* Para el bombeo del riego se utilizó una mini bomba de agua totalmente sumergible con un voltaje de funcionamiento de 2,5v a 6v DC. 
* Permitirá tener un flujo de hasta 2 litros de agua por minuto (de 80 -120l/h).

* Con un motor interno de 0,3A.


# ESPECIFICACIONES TÉCNICAS.

* Voltaje de funcionamiento: 2,5-6V DC.

* Altura bombeo máx.: 40-110CM.

* Caudal bombeo máx.: 80-120 L/H.

* Diámetro salida Exterior: 7,5 MM.

* Diámetro salida Interior: 5 MM.

* Tiempo continuo de trabajo de 500 HORAS.

